--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not WCDBConfig then return end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local tinsert 			= table.insert
local tremove			= table.remove

local T = LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "wcdb", debug )
local LibGUI 	= LibStub( "LibGUI" )

local db

local initialized		= false

local InitializeWindow, ApplyConfigSettings, RevertConfigSettings
local window	= {
	Name		= T["Filters"],
	display		= {},
	W			= {},
}

local whitelist = {}
local blacklist = {}

local currentAbility = {}

local function deepcopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return new_table
    end
    return _copy(object)
end

function InitializeWindow()
	-- If our window already exists, we are all set
	if initialized then return end
	
	local w
	local e	
	
	-- Set our local database
	db = WCDB.db
	
	-- Filter Window
	w = LibGUI( "window", nil, "WCDBWindowDefault" )
	w:ClearAnchors()
	w:Resize( 400, 400 )
	w:Show( true )
	window.W.Filter = w
	
	-- This is the order the windows are displayed to the user
	table.insert( window.display, window.W.Filter )
	
	--
	-- Filter WINDOW
	--
	-- Filter - Subsection Title
    e = window.W.Filter( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Filter, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["Filter"] )
    window.W.Filter.Title_Label = e
    
    -- Filter - Enable Filtering Label
    e = window.W.Filter( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Filter.Title_Label, "bottomleft", "topleft", 5, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Enable Filtering:"] )
    window.W.Filter.EnableFiltering_Label = e
    
    -- Filter - Active Filter Combo
    e = window.W.Filter( "combobox", nil, "WCDBFilterComboBox_ActiveFilter" )
    e:AnchorTo( window.W.Filter.EnableFiltering_Label, "bottomleft", "topleft", 10, 0 )
    e:Clear()
    e:Add( T["None"] )
    e:Add( T["Enable Whitelist"] )
    e:Add( T["Enable Blacklist"] )
    window.W.Filter.SetActiveFilter_Combo = e
    
    -- Filter - Filtered Abilities Label
    e = window.W.Filter( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Filter.EnableFiltering_Label, "bottomleft", "topleft", 0, 30 )
    e:Font( "font_chat_text" )
    e:SetText( T["Filtered Abilities:"] )
    window.W.Filter.FilteredAbilities_Label = e
    
    -- Filter - Filtered Abilities Combo
    e = window.W.Filter( "combobox", nil, "WCDB_ComboBox_DefaultResizable" )
    e:AnchorTo( window.W.Filter.FilteredAbilities_Label, "bottomleft", "topleft", 10, 0 )
    window.W.Filter.FilteredAbilities_Combo = e
    
    -- Filter - Delete Filter Button
    e = window.W.Filter( "Button", nil, "EA_Button_DefaultResizeable" )
    e:Resize( 100 )
    e:Show( true )
    e:AnchorTo( window.W.Filter.FilteredAbilities_Combo, "right", "left", 5, 0 )
    e:SetText( T["Delete"] )
    e.OnLButtonUp = 
		function()
			WCDBConfig_Filters_DeletedSelectedFilter()
		end
    window.W.Filter.DeleteFilter_Button = e
    
    -- Filter - Ability Drop Label
    e = window.W.Filter( "Label" )
    e:Resize( 500, 40 )
    e:Align( "center" )
    e:AnchorTo( window.W.Filter.FilteredAbilities_Combo, "bottomleft", "topleft", 0, 35 )
    e:Font( "font_chat_text" )
    e:Color( 255, 255, 255 )
    e:WordWrap( true )
    e:SetText( T["Drag and drop abilities below and click 'Add Ability' to add them to the current filter."] )
    window.W.Filter.Note_Label = e
    
    -- Filter - Ability Drop Button
    e = window.W.Filter( "Button", nil, "WCDB_Button_Ability" )
    e:Show( true )
    e:AnchorTo( window.W.Filter.Note_Label, "bottom", "top", 0, 25 )
    --e:SetText( T["Delete"] )
    --e:Texture( "", 0, 0 )
    e.OnLButtonUp = 
		function()
			WCDBConfig_Filters_OnLButtonUp_Ability()
		end
	e.OnRButtonUp = 
		function()
			WCDBConfig_Filters_ClearAbility()
		end
    window.W.Filter.AbilityDrop_Button = e
    
    -- Filter - Add Ability Button
    e = window.W.Filter( "Button", nil, "EA_Button_DefaultResizeable" )
    e:Resize( 200 )
    e:Show( true )
    e:AnchorTo( window.W.Filter.AbilityDrop_Button, "bottom", "top", 0, 20 )
    e:SetText( T["Add Ability"] )
    e.OnLButtonUp = 
		function()
			WCDBConfig_Filters_AddAbilityToFilter()
		end
    window.W.Filter.AddAbility_Button = e
    
    initialized = true
end

function ApplyConfigSettings()
	db.general.enablewhitelist = ( window.W.Filter.SetActiveFilter_Combo:SelectedIndex() == 2 )
	db.general.enableblacklist = ( window.W.Filter.SetActiveFilter_Combo:SelectedIndex() == 3 )
	
	db.whitelist = deepcopy( whitelist )
	db.blacklist = deepcopy( blacklist )
end

function RevertConfigSettings()
	-- Update our lists with the current lists
	whitelist = deepcopy( db.whitelist )
	blacklist = deepcopy( db.blacklist )

	if( db.general.enablewhitelist ) then
		window.W.Filter.SetActiveFilter_Combo:SelectIndex( 2 )
	elseif( db.general.enableblacklist ) then
		window.W.Filter.SetActiveFilter_Combo:SelectIndex( 3 )
	else
		window.W.Filter.SetActiveFilter_Combo:SelectIndex( 1 )
	end
	
	WCDBConfig_Filters_ClearAbility()

	WCDBConfig_Filters_UpdateEnableBits()
	
	WCDBConfig_Filters_UpdateAbilityCombo()
end

function WCDBConfig_Filters_OnActiveFilterChanged()
	WCDBConfig_Filters_UpdateEnableBits()
	WCDBConfig_Filters_UpdateAbilityCombo()
end

function WCDBConfig_Filters_UpdateAbilityCombo()
	local abilityList = {}
	
	if( window.W.Filter.SetActiveFilter_Combo:SelectedIndex() == 2 ) then
		abilityList = whitelist
	elseif( window.W.Filter.SetActiveFilter_Combo:SelectedIndex() == 3 ) then
		abilityList = blacklist
	else
		abilityList = nil
	end
	
	window.W.Filter.FilteredAbilities_Combo:Clear()
	if abilityList ~= nil then
		local populated = false
		for k,v in pairs(abilityList) do
			local abilityData = Player.GetAbilityData( v )
			if( abilityData ~= nil ) then
	        	window.W.Filter.FilteredAbilities_Combo:Add(abilityData.name)
				populated = true
			end
	    end
	    
	    if populated then
	        window.W.Filter.FilteredAbilities_Combo:SelectIndex( 1 )
	    end
	end
end

function WCDBConfig_Filters_UpdateEnableBits()
	local bEnableFilterCombo = (window.W.Filter.SetActiveFilter_Combo:SelectedIndex() ~= 1)
	 
	window.W.Filter.FilteredAbilities_Combo:SetEnabled( bEnableFilterCombo )
	window.W.Filter.DeleteFilter_Button:SetEnabled( bEnableFilterCombo )
	window.W.Filter.AbilityDrop_Button:SetEnabled( bEnableFilterCombo )
	window.W.Filter.AddAbility_Button:SetEnabled( bEnableFilterCombo )
end

function WCDBConfig_Filters_AddAbilityToFilter()
	local abilityList
	local found = false
	
	if( window.W.Filter.SetActiveFilter_Combo:SelectedIndex() == 2 ) then
		abilityList = whitelist
	elseif( window.W.Filter.SetActiveFilter_Combo:SelectedIndex() == 3 ) then
		abilityList = blacklist
	end
	
	-- Check to see if the current ability already exists in in the filter
	for k,v in ipairs( abilityList ) do
		if( v == currentAbility.id ) then
			found = true
			break
		end
	end
	
	-- If we did not find the ability, add it to the list
	if not found then
		tinsert( abilityList, #abilityList + 1, currentAbility.id )
		window.W.Filter.FilteredAbilities_Combo:Add(currentAbility.name)
		window.W.Filter.FilteredAbilities_Combo:SelectIndex( #abilityList )	
	end
	
	WCDBConfig_Filters_ClearAbility()
end

function WCDBConfig_Filters_DeletedSelectedFilter()
	local idx = window.W.Filter.FilteredAbilities_Combo:SelectedIndex()
	
	if idx ~= nil and idx > 0 then
		local abilityList
		
		if( window.W.Filter.SetActiveFilter_Combo:SelectedIndex() == 2 ) then
			abilityList = whitelist
		elseif( window.W.Filter.SetActiveFilter_Combo:SelectedIndex() == 3 ) then
			abilityList = blacklist
		end
	
		tremove( abilityList, idx )
		
		WCDBConfig_Filters_UpdateAbilityCombo()
	end	
end

function WCDBConfig_Filters_ClearAbility()
	DynamicImageSetTexture( window.W.Filter.AbilityDrop_Button.name .. "SquareIcon", "", 0, 0 )
	WindowSetShowing(window.W.Filter.AbilityDrop_Button.name.."SquareIcon", true)
    WindowSetShowing(window.W.Filter.AbilityDrop_Button.name.."CircleIcon", false)
	
    LabelSetText( window.W.Filter.AbilityDrop_Button.name .."Desc", L"" )
    LabelSetText( window.W.Filter.AbilityDrop_Button.name .."DescPath", L"" )
    LabelSetText( window.W.Filter.AbilityDrop_Button.name .. "DescType", L"" )
    
    currentAbility = nil
end

function WCDBConfig_Filters_OnLButtonUp_Ability()
	if( Cursor.IconOnCursor() ) then
		-- If the cursor data came from the action list	
		if( Cursor.Data.Source == Cursor.SOURCE_ACTION_LIST ) then
			local abilityData = nil
			
			-- Attempt to retrieve the ability data
			local abilityData = Player.GetAbilityData( Cursor.Data.ObjectId )
			
			if( abilityData ~= nil and 
					( abilityData.abilityType == GameData.AbilityType.STANDARD or 
					abilityData.abilityType == GameData.AbilityType.MORALE or
					abilityData.abilityType == GameData.AbilityType.PET ) ) then
				
				local texture, dx, dy = GetIconData( tonumber( abilityData.iconNum ) )
				
				-- Set the texture to that of the ability dropped on us
				DynamicImageSetTexture( window.W.Filter.AbilityDrop_Button.name .. "SquareIcon", texture, dx, dy )
				
				WindowSetShowing(window.W.Filter.AbilityDrop_Button.name.."SquareIcon", mode ~= GameData.AbilityType.MORALE)
                WindowSetShowing(window.W.Filter.AbilityDrop_Button.name.."CircleIcon", mode == GameData.AbilityType.MORALE)
				
				LabelSetText( window.W.Filter.AbilityDrop_Button.name .."Desc", GetStringFormat( StringTables.Default.LABEL_ABILITIES_WINDOW_ABILITY_NAME_FORMAT, {abilityData.name} ) )
    			LabelSetText( window.W.Filter.AbilityDrop_Button.name .."DescPath", DataUtils.GetAbilitySpecLine( abilityData ) )
    			local abilityTypeText = DataUtils.GetAbilityTypeText( abilityData )
    			LabelSetText( window.W.Filter.AbilityDrop_Button.name .. "DescType", abilityTypeText )
    			local _, _, _, r, g, b = DataUtils.GetAbilityTypeTextureAndColor( abilityData )
    			LabelSetTextColor( window.W.Filter.AbilityDrop_Button.name .. "DescType", r, g, b)
				
				-- Store the ability for when the add button is pressed
				currentAbility = abilityData
				
				-- Clear the cursor
				Cursor.Clear()
			end
		end
	end
end



window.Initialize	= InitializeWindow
window.Apply		= ApplyConfigSettings
window.Revert		= RevertConfigSettings
window.Index		= WCDBConfig.RegisterWindow( window )
