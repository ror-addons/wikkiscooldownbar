--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not WCDBConfig then return end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "wcdb", debug )
local LibGUI 	= LibStub( "LibGUI" )

local db

local initialized		= false

local fgRed, fgGreen, fgBlue, fgAlpha
local bgRed, bgGreen, bgBlue, bgAlpha
local fontRed, fontGreen, fontBlue, fontAlpha

local InitializeWindow, ApplyConfigSettings, RevertConfigSettings
local window	= {
	Name		= T["Pet Actions"],
	display		= {},
	W			= {},
}
local textures					= WCDBConstants.Textures
local textureNames				= WCDBConstants.TextureNames

function InitializeWindow()
	-- If our window already exists, we are all set
	if initialized then return end
	
	local w
	local e	
	
	-- Set our local database
	db = WCDB.db
	
	-- General Window
	w = LibGUI( "window", nil, "WCDBWindowDefault" )
	w:ClearAnchors()
	w:Resize( 400, 130 )
	w:Show( true )
	window.W.General = w
	
	-- Colors Window
	w = LibGUI( "window", nil, "WCDBWindowDefault" )
    w:Resize( 400, 675 )
	w:Show( true )
	window.W.Colors = w
	
	-- This is the order the windows are displayed to the user
	table.insert( window.display, window.W.General )
	table.insert( window.display, window.W.Colors )
	
	--
	-- General WINDOW
	--
	-- General - Subsection Title
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["General"] )
    window.W.General.Title_Label = e
    
    -- General - Enable Pet Actions Checkbox
	e = window.W.General( "Checkbox" )
	e:AnchorTo( window.W.General.Title_Label, "bottomleft", "topleft", 10, 5 )
	window.W.General.EnablePetActions_Checkbox = e
    
    -- General - Enable Actions Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.EnablePetActions_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Enable pet action cooldown monitoring"] )
    window.W.General.EnablePetActions_Label = e
    
    -- General - Show Icon Checkbox
	e = window.W.General( "Checkbox" )
	e:AnchorTo( window.W.General.EnablePetActions_Label, "bottomleft", "topleft", 0, 5 )
	window.W.General.ShowIcon_Checkbox = e
    
    -- General - Show Icon Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.ShowIcon_Checkbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Show action icon"] )
    window.W.General.ShowIcon_Label = e
    
    -- General - Minimum Cooldown Label
    e = window.W.General( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.General.ShowIcon_Checkbox, "bottomleft", "topleft", 0, 5 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Minimum cooldown time to display:"] )
    window.W.General.MinimumCooldown_Label = e
    
    -- General - Minimum Cooldown Textbox
    e = window.W.General( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.General.MinimumCooldown_Label, "right", "left", 10, 0 )
    window.W.General.MinimumCooldown_Textbox = e
    
    -- General - Units Label
    e = window.W.General( "Label" )
    e:Resize( 65 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.MinimumCooldown_Textbox, "right", "left", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["(sec)"] )
    window.W.General.Units_Label = e
    
    --
	-- COLORS WINDOW ELEMENTS
	--
	-- Colors - Subsection Title
    e = window.W.Colors( "Label" )
    e:Resize( 550 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.Colors, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["Colors"] )
    window.W.Colors.Title_Label = e

	-- Colors - Foreground Texture Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 450 )
    e:Align( "center" )
    e:AnchorTo( window.W.Colors, "top", "top", 0, 30 )
    e:Font( "font_chat_text" )
    e:SetText( T["Foreground Texture Color"] )
    window.W.Colors.ForegroundTextureColor_Label = e
    
    -- Colors - Foreground Color Preview Image
    e = window.W.Colors( "Image" )
    e:Resize( 100, 25 )
    e:AnchorTo( window.W.Colors.ForegroundTextureColor_Label, "bottom", "top", 0, 0 )
    e:Tint( 255, 255, 255 )
    e:UnregisterDefaultEvents()
    window.W.Colors.ForegroundPreview_Image = e
    
    -- Colors - Foreground Red Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ForegroundPreview_Image, "bottom", "top", 0, 8 )
    e:SetRange( 0,255 )
    window.W.Colors.ForegroundRed_Slider = e
    
    -- Colors - Foreground Red Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ForegroundRed_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Red"] )
    window.W.Colors.ForegroundRed_Label = e
    
    -- Colors - Foreground Red Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ForegroundRed_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ForegroundRed_Textbox = e
    
    -- Colors - Foreground Green Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ForegroundRed_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.ForegroundGreen_Slider = e
    
    -- Colors - Foreground Green Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ForegroundGreen_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Green"] )
    window.W.Colors.ForegroundGreen_Label = e
    
    -- Colors - Foreground Green Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ForegroundGreen_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ForegroundGreen_Textbox = e
    
    -- Colors - Foreground Blue Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ForegroundGreen_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.ForegroundBlue_Slider = e
	
	-- Colors - Foreground Blue Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ForegroundBlue_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Blue"] )
    window.W.Colors.ForegroundBlue_Label = e
    
    -- Colors - Foreground Blue Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ForegroundBlue_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ForegroundBlue_Textbox = e
    
    -- Colors - Foreground Alpha Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.ForegroundBlue_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0, 1 )
    window.W.Colors.ForegroundAlpha_Slider = e
    
    -- Colors - Foreground Alpha Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.ForegroundAlpha_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Alpha"] )
    window.W.Colors.ForegroundAlpha_Label = e
    
    -- Colors - Foreground Alpha Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.ForegroundAlpha_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.ForegroundAlpha_Textbox = e
    
    -- Colors - Background Texture Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 450 )
    e:Align( "center" )
    e:AnchorTo( window.W.Colors.ForegroundAlpha_Slider, "bottom", "top", 0, 10 )
    e:Font( "font_chat_text" )
    e:SetText( T["Background Texture Color"] )
    window.W.Colors.BackgroundTextureColor_Label = e
    
    -- Colors - Background Color Preview Image
    e = window.W.Colors( "Image" )
    e:Resize( 100, 25 )
    e:AnchorTo( window.W.Colors.BackgroundTextureColor_Label, "bottom", "top", 0, 0 )
    e:Tint( 255, 255, 255 )
    e:UnregisterDefaultEvents()
    window.W.Colors.BackgroundPreview_Image = e
    
    -- Colors - Background Red Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.BackgroundPreview_Image, "bottom", "top", 0, 8 )
    e:SetRange( 0,255 )
    window.W.Colors.BackgroundRed_Slider = e
    
    -- Colors - Background Red Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.BackgroundRed_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Red"] )
    window.W.Colors.BackgroundRed_Label = e
    
    -- Colors - Background Red Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.BackgroundRed_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.BackgroundRed_Textbox = e
    
    -- Colors - Background Green Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.BackgroundRed_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.BackgroundGreen_Slider = e
    
    -- Colors - Background Green Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.BackgroundGreen_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Green"] )
    window.W.Colors.BackgroundGreen_Label = e
    
    -- Colors - Background Green Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.BackgroundGreen_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.BackgroundGreen_Textbox = e
    
    -- Colors - Background Blue Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.BackgroundGreen_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.BackgroundBlue_Slider = e
	
	-- Colors - Background Blue Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.BackgroundBlue_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Blue"] )
    window.W.Colors.BackgroundBlue_Label = e
    
    -- Colors - Background Blue Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.BackgroundBlue_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.BackgroundBlue_Textbox = e
    
    -- Colors - Background Alpha Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.BackgroundBlue_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0, 1 )
    window.W.Colors.BackgroundAlpha_Slider = e
    
    -- Colors - Background Alpha Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.BackgroundAlpha_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Alpha"] )
    window.W.Colors.BackgroundAlpha_Label = e
    
    -- Colors - Background Alpha Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.BackgroundAlpha_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.BackgroundAlpha_Textbox = e
    
    -- Colors - Font Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 450 )
    e:Align( "center" )
    e:AnchorTo( window.W.Colors.BackgroundAlpha_Slider, "bottom", "top", 0, 10 )
    e:Font( "font_chat_text" )
    e:SetText( T["Font Color"] )
    window.W.Colors.FontColor_Label = e
    
    -- Colors - Font Red Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.FontColor_Label, "bottom", "top", 0, 8 )
    e:SetRange( 0,255 )
    window.W.Colors.FontRed_Slider = e
    
    -- Colors - Font Red Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.FontRed_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Red"] )
    window.W.Colors.FontRed_Label = e
    
    -- Colors - Font Red Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.FontRed_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.FontRed_Textbox = e
    
    -- Colors - Font Green Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.FontRed_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.FontGreen_Slider = e
    
    -- Colors - Font Green Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.FontGreen_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Green"] )
    window.W.Colors.FontGreen_Label = e
    
    -- Colors - Font Green Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.FontGreen_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.FontGreen_Textbox = e
    
    -- Colors - Font Blue Color Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.FontGreen_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0,255 )
    window.W.Colors.FontBlue_Slider = e
	
	-- Colors - Font Blue Color Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.FontBlue_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Blue"] )
    window.W.Colors.FontBlue_Label = e
    
    -- Colors - Font Blue Color Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.FontBlue_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.FontBlue_Textbox = e
    
    -- Colors - Font Alpha Slider
    e = window.W.Colors( "Slider" )
    e:AnchorTo( window.W.Colors.FontBlue_Slider, "bottom", "top", 0, 5 )
    e:SetRange( 0, 1 )
    window.W.Colors.FontAlpha_Slider = e
    
    -- Colors - Font Alpha Label
    e = window.W.Colors( "Label" )
    e:Resize( 200 )
    e:AnchorTo( window.W.Colors.FontAlpha_Slider, "left", "right", -15, 0)
    e:Font( "font_chat_text" )
    e:Align( "rightcenter" )
    e:SetText( T["Alpha"] )
    window.W.Colors.FontAlpha_Label = e
    
    -- Colors - Font Alpha Textbox
    e = window.W.Colors( "Textbox" )
    e:Resize( 65 )
    e:AnchorTo( window.W.Colors.FontAlpha_Slider, "right", "left", 10, 0 )
    e:SetText( 255 )
    window.W.Colors.FontAlpha_Textbox = e
    
    initialized = true
end

function ApplyConfigSettings()
	--
	-- General
	--
	db.abilities.pet.enabled 		= window.W.General.EnablePetActions_Checkbox:GetValue()
	db.abilities.pet.icon.show 		= window.W.General.ShowIcon_Checkbox:GetValue()
	db.abilities.pet.mincooldown 	= tonumber( window.W.General.MinimumCooldown_Textbox:GetText() ) or 4
	
	--
	-- Colors
	--
	db.abilities.pet.fg.color		= { fgRed, fgGreen, fgBlue }
	db.abilities.pet.fg.alpha		= fgAlpha
	db.abilities.pet.bg.color		= { bgRed, bgGreen, bgBlue }
	db.abilities.pet.bg.alpha		= bgAlpha
	db.abilities.pet.font.color		= { fontRed, fontGreen, fontBlue }
	db.abilities.pet.font.alpha		= fontAlpha
end

function RevertConfigSettings()
	--
	-- General
	--
	window.W.General.EnablePetActions_Checkbox:SetValue( db.abilities.pet.enabled  )
	window.W.General.ShowIcon_Checkbox:SetValue( db.abilities.pet.icon.show )
	window.W.General.MinimumCooldown_Textbox:SetText( ( towstring( db.abilities.pet.mincooldown ) ) )
	
	--
	-- Colors
	--
	fgRed, fgGreen, fgBlue 			= unpack( db.abilities.pet.fg.color )
	fgAlpha							= db.abilities.pet.fg.alpha
	window.W.Colors.ForegroundPreview_Image:Tint( fgRed, fgGreen, fgBlue )
	window.W.Colors.ForegroundPreview_Image:Alpha( fgAlpha )
	window.W.Colors.ForegroundRed_Slider:SetValue( fgRed )
	window.W.Colors.ForegroundRed_Textbox:SetText( towstring( fgRed ) )
	window.W.Colors.ForegroundGreen_Slider:SetValue( fgGreen )
    window.W.Colors.ForegroundGreen_Textbox:SetText( towstring( fgGreen ) )
    window.W.Colors.ForegroundBlue_Slider:SetValue( fgBlue )
    window.W.Colors.ForegroundBlue_Textbox:SetText( towstring( fgBlue ) )
    window.W.Colors.ForegroundAlpha_Slider:SetValue( fgAlpha )
	window.W.Colors.ForegroundAlpha_Textbox:SetText( wstring.format( L"%.2f", towstring( fgAlpha ) ) )
	
	bgRed, bgGreen, bgBlue 			= unpack( db.abilities.pet.bg.color )
	bgAlpha							= db.abilities.pet.bg.alpha
	window.W.Colors.BackgroundPreview_Image:Tint( bgRed, bgGreen, bgBlue )
	window.W.Colors.BackgroundPreview_Image:Alpha( bgAlpha )
	window.W.Colors.BackgroundRed_Slider:SetValue( bgRed )
	window.W.Colors.BackgroundRed_Textbox:SetText( towstring( bgRed ) )
	window.W.Colors.BackgroundGreen_Slider:SetValue( bgGreen )
    window.W.Colors.BackgroundGreen_Textbox:SetText( towstring( bgGreen ) )
    window.W.Colors.BackgroundBlue_Slider:SetValue( bgBlue )
    window.W.Colors.BackgroundBlue_Textbox:SetText( towstring( bgBlue ) )
    window.W.Colors.BackgroundAlpha_Slider:SetValue( bgAlpha )
	window.W.Colors.BackgroundAlpha_Textbox:SetText( wstring.format( L"%.2f", towstring( bgAlpha ) ) )
	
	fontRed, fontGreen, fontBlue 	= unpack( db.abilities.pet.font.color )
	fontAlpha						= db.abilities.pet.font.alpha
	window.W.Colors.FontColor_Label:Color( fontRed, fontGreen, fontBlue )
	WindowSetFontAlpha( window.W.Colors.FontColor_Label.name, fontAlpha )
	window.W.Colors.FontRed_Slider:SetValue( fontRed )
	window.W.Colors.FontRed_Textbox:SetText( towstring( fontRed ) )
	window.W.Colors.FontGreen_Slider:SetValue( fontGreen )
    window.W.Colors.FontGreen_Textbox:SetText( towstring( fontGreen ) )
    window.W.Colors.FontBlue_Slider:SetValue( fontBlue )
    window.W.Colors.FontBlue_Textbox:SetText( towstring( fontBlue ) )
    window.W.Colors.FontAlpha_Slider:SetValue( fontAlpha )
	window.W.Colors.FontAlpha_Textbox:SetText( wstring.format( L"%.2f", towstring( fontAlpha ) ) )
end

function WCDBConfig_PetActions_OnUpdate()
	if( WCDBConfig.IsShowing() and ( WCDBConfig.GetActiveConfigWindowIndex() == window.Index ) and initialized ) then
		local updated = false
		local ret = false
		
		--
		-- Color Colors
		--
		-- 	Get the slider values
		ret, fgRed = WCDBConfig.UpdateColorSelection( fgRed, window.W.Colors.ForegroundRed_Slider, window.W.Colors.ForegroundRed_Textbox )
		updated = updated or ret
		
		ret, fgGreen = WCDBConfig.UpdateColorSelection( fgGreen, window.W.Colors.ForegroundGreen_Slider, window.W.Colors.ForegroundGreen_Textbox )
		updated = updated or ret
		
		ret, fgBlue = WCDBConfig.UpdateColorSelection( fgBlue, window.W.Colors.ForegroundBlue_Slider, window.W.Colors.ForegroundBlue_Textbox )
		updated = updated or ret
		
		ret, fgAlpha = WCDBConfig.UpdateAlphaSelection( fgAlpha, window.W.Colors.ForegroundAlpha_Slider, window.W.Colors.ForegroundAlpha_Textbox )
		updated = updated or ret
		
		-- Update our preview color
		if( updated ) then
			window.W.Colors.ForegroundPreview_Image:Tint( fgRed, fgGreen, fgBlue )
			window.W.Colors.ForegroundPreview_Image:Alpha( fgAlpha )
			updated = false
		end
		
		-- 	Get the slider values
		ret, bgRed = WCDBConfig.UpdateColorSelection( bgRed, window.W.Colors.BackgroundRed_Slider, window.W.Colors.BackgroundRed_Textbox )
		updated = updated or ret
		
		ret, bgGreen = WCDBConfig.UpdateColorSelection( bgGreen, window.W.Colors.BackgroundGreen_Slider, window.W.Colors.BackgroundGreen_Textbox )
		updated = updated or ret
		
		ret, bgBlue = WCDBConfig.UpdateColorSelection( bgBlue, window.W.Colors.BackgroundBlue_Slider, window.W.Colors.BackgroundBlue_Textbox )
		updated = updated or ret
		
		ret, bgAlpha = WCDBConfig.UpdateAlphaSelection( bgAlpha, window.W.Colors.BackgroundAlpha_Slider, window.W.Colors.BackgroundAlpha_Textbox )
		updated = updated or ret
		
		-- Update our preview color
		if( updated ) then
			window.W.Colors.BackgroundPreview_Image:Tint( bgRed, bgGreen, bgBlue )
			window.W.Colors.BackgroundPreview_Image:Alpha( bgAlpha )
			updated = false
		end
		
		-- 	Get the slider values
		ret, fontRed = WCDBConfig.UpdateColorSelection( fontRed, window.W.Colors.FontRed_Slider, window.W.Colors.FontRed_Textbox )
		updated = updated or ret
		
		ret, fontGreen = WCDBConfig.UpdateColorSelection( fontGreen, window.W.Colors.FontGreen_Slider, window.W.Colors.FontGreen_Textbox )
		updated = updated or ret
		
		ret, fontBlue = WCDBConfig.UpdateColorSelection( fontBlue, window.W.Colors.FontBlue_Slider, window.W.Colors.FontBlue_Textbox )
		updated = updated or ret
		
		ret, fontAlpha = WCDBConfig.UpdateAlphaSelection( fontAlpha, window.W.Colors.FontAlpha_Slider, window.W.Colors.FontAlpha_Textbox )
		updated = updated or ret
		
		-- Update our preview color
		if( updated ) then
			window.W.Colors.FontColor_Label:Color( fontRed, fontGreen, fontBlue )
			WindowSetFontAlpha( window.W.Colors.FontColor_Label.name, fontAlpha )
			updated = false
		end
	end	
end



window.Initialize	= InitializeWindow
window.Apply		= ApplyConfigSettings
window.Revert		= RevertConfigSettings
window.Index		= WCDBConfig.RegisterWindow( window )
