--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not WCDBConfig then return end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "wcdb", debug )
local LibGUI 	= LibStub( "LibGUI" )

local db

local initialized		= false

local InitializeWindow, ApplyConfigSettings, RevertConfigSettings
local window	= {
	Name		= T["General"],
	display		= {},
	W			= {},
}
local textures					= WCDBConstants.Textures
local textureNames				= WCDBConstants.TextureNames

local growthDirections			= WCDBConstants.GROWTH_TYPES
local sortTypes					= WCDBConstants.SORT_TYPES
local sortDirections			= WCDBConstants.ORDER_TYPES
local depleteDirections			= WCDBConstants.DEPLETION_TYPES

function InitializeWindow()
	-- If our window already exists, we are all set
	if initialized then return end
	
	local w
	local e	
	
	-- Set our local database
	db = WCDB.db
	
	-- General Window
	w = LibGUI( "window", nil, "WCDBWindowDefault" )
	w:ClearAnchors()
	w:Resize( 400, 400 )
	w:Show( true )
	window.W.General = w
	
	-- This is the order the windows are displayed to the user
	table.insert( window.display, window.W.General )
	
	--
	-- GENERAL WINDOW
	--
	-- General - Subsection Title
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General, "topleft", "topleft", 15, 10 )
    e:Font( "font_clear_medium_bold" )
    e:SetText( T["General"] )
    window.W.General.Title_Label = e
    
    -- General - Bar Texture Label
    e = window.W.General( "Label" )
    e:Resize( 450 )
    e:Align( "leftcenter" )
    e:AnchorTo( window.W.General.Title_Label, "bottomleft", "topleft", 10, 0 )
    e:Font( "font_chat_text" )
    e:SetText( T["Cooldown Bar Texture:"] )
    window.W.General.CooldownBarTexture_Label = e
    
    -- General - Bar Texture Combo
    e = window.W.General( "combobox", nil, "WCDBComboBox" )
    e:AnchorTo( window.W.General.CooldownBarTexture_Label, "bottomleft", "topleft", 0, 0 )
    for _, texture in ipairs( textureNames ) 
	do 
		e:Add( texture )
	end
    window.W.General.CooldownBarTexture_Combo = e
    
    -- General - Growth Direction Label
    e = window.W.General( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.General.CooldownBarTexture_Combo, "bottomleft", "topleft", 0, 5 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Growth Direction:"] )
    window.W.General.GrowthDirection_Label = e
    
    -- General - Growth Direction Combo
    e = window.W.General( "Combobox" )
    e:AnchorTo( window.W.General.GrowthDirection_Label, "bottomleft", "topleft", 0, 0 )
    for _, v in ipairs( growthDirections ) 
	do 
		e:Add( v.name )
	end
    window.W.General.GrowthDirection_Combo = e
    
    -- General - Sort Type Label
    e = window.W.General( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.General.GrowthDirection_Combo, "bottomleft", "topleft", 0, 5 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Sort Type:"] )
    window.W.General.SortType_Label = e
    
    -- General - Sort Type Combo
    e = window.W.General( "Combobox" )
    e:AnchorTo( window.W.General.SortType_Label, "bottomleft", "topleft", 0, 0 )
    for _, v in ipairs( sortTypes ) 
	do 
		e:Add( v.name )
	end
    window.W.General.SortType_Combo = e
    
    -- General - Sort Direction Label
    e = window.W.General( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.General.SortType_Combo, "bottomleft", "topleft", 0, 5 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Sort Direction:"] )
    window.W.General.SortDirection_Label = e
    
    -- General - Sort Direction Combo
    e = window.W.General( "Combobox" )
    e:AnchorTo( window.W.General.SortDirection_Label, "bottomleft", "topleft", 0, 0 )
    for _, v in ipairs( sortDirections ) 
	do 
		e:Add( v.name )
	end
    window.W.General.SortDirection_Combo = e
    
    -- General - Depletion Direction Label
    e = window.W.General( "Label" )
    e:Resize( 300 )
    e:AnchorTo( window.W.General.SortDirection_Combo, "bottomleft", "topleft", 0, 5 )
    e:Font( "font_chat_text" )
    e:Align( "leftcenter" )
    e:SetText( T["Bar Depletion Direction:"] )
    window.W.General.DepletionDirection_Label = e
    
    -- General - Depletion Direction Combo
    e = window.W.General( "Combobox" )
    e:AnchorTo( window.W.General.DepletionDirection_Label, "bottomleft", "topleft", 0, 0 )
    for _, v in ipairs( depleteDirections ) 
	do 
		e:Add( v.name )
	end
    window.W.General.DepletionDirection_Combo = e
    
    initialized = true
end

function ApplyConfigSettings()
	db.bar.texture 					= textures[window.W.General.CooldownBarTexture_Combo:SelectedIndex()]
	db.bar.grow						= window.W.General.GrowthDirection_Combo:SelectedIndex()
	db.bar.sorttype					= window.W.General.SortType_Combo:SelectedIndex()
	db.bar.sort						= window.W.General.SortDirection_Combo:SelectedIndex()
	db.bar.depletion				= window.W.General.DepletionDirection_Combo:SelectedIndex()
end

function RevertConfigSettings()
	window.W.General.CooldownBarTexture_Combo:SelectIndex( textures[db.bar.texture] )
	window.W.General.GrowthDirection_Combo:SelectIndex( db.bar.grow )
	window.W.General.SortType_Combo:SelectIndex( db.bar.sorttype )
	window.W.General.SortDirection_Combo:SelectIndex( db.bar.sort )
	window.W.General.DepletionDirection_Combo:SelectIndex( db.bar.depletion )
end

window.Initialize	= InitializeWindow
window.Apply		= ApplyConfigSettings
window.Revert		= RevertConfigSettings
window.Index		= WCDBConfig.RegisterWindow( window )