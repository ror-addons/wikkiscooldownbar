--
-- This file adapted from SquaredProfiles.lua, from Aiiane's Squared, to work with this addon
--
local WCDB = WCDB

-- If it doesn't already exist, make it
if not WCDB.Profiles then WCDB.Profiles = {active=0,names={},data={},perchar={}} end

-- Make utility functions local for performance
local pairs 			= pairs
local unpack 			= unpack
local tonumber			= tonumber
local tostring 			= tostring
local towstring 		= towstring
local max 				= math.max
local min 				= math.min
local wstring_sub 		= wstring.sub
local wstring_format 	= wstring.format
local tinsert 			= table.insert
local tremove 			= table.remove

-- generic deepcopy
local function deepcopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return new_table
    end
    return _copy(object)
end

-- This function takes a defaults table and a settings, and verifies that all 
-- keys that exist in the defaults exist in the settings.
local function updateSettings(defaults, settings)
	if type(defaults) ~= "table" or type(settings) ~= "table" then return end
	for key, value in pairs( defaults ) do
		if type(value) ~= "table" and value ~= nil then
			-- Check if the key exists in settings
			if settings[key] == nil then
				settings[key] = value
			end
		else
			-- Create the table if it doesnt exist, or if its type is not of table
			if settings[key] == nil or type(settings[key]) ~= "table" then
				settings[key] = {}
			end
			
			updateSettings( value, settings[key] )
		end
	end	
end

local function GetCurrentSlot()
	for k,v in pairs(GameData.Account.CharacterSlot) do
		if v.Name == GameData.Player.name then
			return k
		end
	end
	return nil
end

function WCDB.Set(key, value)
	if key then
		WCDB.Profiles.data[WCDB.Profiles.active][key] = value
	end
end

function WCDB.Get( key )
	if key then
		return WCDB.Profiles.data[WCDB.Profiles.active][key]
	end
	return nil
end

function WCDB.ResetActiveProfile()
	if WCDB.Profiles and WCDB.Profiles.data and WCDB.Profiles.data[WCDB.Profiles.active] then
		WCDB.Profiles.data[WCDB.Profiles.active] = deepcopy(WCDB.DefaultSettings)
		WCDB.OnProfileChanged()
	end
end

-- Initialize the profile system (called from WCDB.OnLoad)
function WCDB.InitProfiles()
    if WCDB.Profiles.active == 0 then
		-- Empty profiles table - initialize the 'Default' profile
		WCDB.Profiles.active = WCDB.AddProfile(L"Default")
		local slot = GetCurrentSlot()
		if not slot then d("Couldn't figure out what character slot this is!") return end
		WCDB.Profiles.perchar[slot] = L"Default"
	else
		-- Non-empty profiles table - check for smart activation
		local slot = GetCurrentSlot()
		local profile = WCDB.Profiles.perchar[slot]
		if profile then
			WCDB.ActivateProfile(profile)
		else 
			WCDB.Profiles.perchar[slot] = L"Default"
			WCDB.ActivateProfile(L"Default")
		end
	end
end

function WCDB.AddProfile(name, source)
	local sourceId = nil
	
	-- First check to see if we're adding a profile which already exists,
	-- if we are then just return that profile's id
	for k,v in ipairs(WCDB.Profiles.names) do
		if v == name then return k end
		-- Might as well find the source Id while we're at it
		if v == source then sourceId = k end
	end
	
	-- If it doesn't exist, add it
	tinsert(WCDB.Profiles.names, name)
	local data = WCDB.GetProfileData(sourceId)
	if data then
		tinsert(WCDB.Profiles.data, data)
	else
		tinsert(WCDB.Profiles.data, deepcopy(WCDB.DefaultSettings))
	end
	
	return #WCDB.Profiles.names
end

function WCDB.GetProfileData(source)
	-- If specified by name, look up the id
	if type(source) == "wstring" then
		for k,v in ipairs(WCDB.Profiles.names) do
			if v == source then
				source = k
				break
			end
		end
	end
	
	if type(source) == "number" then
		return deepcopy(WCDB.Profiles.data[source])
	else
		-- invalid profile
		return nil
	end
end

function WCDB.GetProfileDataForUse(source)
	-- If specified by name, look up the id
	if type(source) == "wstring" then
		for k,v in ipairs(WCDB.Profiles.names) do
			if v == source then
				source = k
				break
			end
		end
	end
	
	if type(source) == "number" then
		return WCDB.Profiles.data[source]
	else
		-- invalid profile
		return nil
	end
end

function WCDB.ActivateProfile(source, force)
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(WCDB.Profiles.names) do
			if v == source then source = k end
		end
	end
	
	if type(source) ~= "number" then return end
	local sourceName = WCDB.Profiles.names[source]
	if not force and WCDB.Profiles.active == source then return end
	
	local data = WCDB.GetProfileData(source)
	if not data then return end
	WCDB.Profiles.active = source
	
	-- Make sure the profile is up to date
	updateSettings( WCDB.DefaultSettings, WCDB.Profiles.data[WCDB.Profiles.active] )
	
	WCDB.OnProfileChanged()
	
	local slot = GetCurrentSlot()
	if slot then
		WCDB.Profiles.perchar[slot] = sourceName
	end

	return source
end

function WCDB.CopyProfileToActive( source )
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(WCDB.Profiles.names) do
			if v == source then source = k end
		end
	end
	if type(source) ~= "number" then return end
	
	if WCDB.Profiles and WCDB.Profiles.data and WCDB.Profiles.data[source] and WCDB.Profiles.data[WCDB.Profiles.active] then
		WCDB.Profiles.data[WCDB.Profiles.active] = deepcopy(WCDB.Profiles.data[source])
		
		updateSettings( WCDB.DefaultSettings, WCDB.Profiles.data[WCDB.Profiles.active] )
		
		WCDB.OnProfileChanged()
	end
end

function WCDB.GetProfileList()
	return deepcopy(WCDB.Profiles.names)
end

function WCDB.GetCurrentProfile()
	return WCDB.Profiles.active, WCDB.Profiles.names[WCDB.Profiles.active]
end

function WCDB.DeleteProfile(source)
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(WCDB.Profiles.names) do
			if v == source then source = k end
		end
	end
	
	if type(source) ~= "number" then return end
	
	-- Don't allow people to delete the default profile, or a nonexistant one
	if source < 2 or not WCDB.Profiles.names[source] then return end
	
	local sourceName = WCDB.Profiles.names[source]
	
	-- Remove the profile
	tremove(WCDB.Profiles.names, source)
	tremove(WCDB.Profiles.data, source)
	
	-- Swap any perchars which were the deleted profile back to default
	local slot = GetCurrentSlot()
	for k,v in pairs(WCDB.Profiles.perchar) do
		if v == sourceName then
			WCDB.Profiles.perchar[k] = L"Default"
			-- If our current character was set to the deleted profile, activate default
			if k == slot then
				WCDB.ActivateProfile(L"Default")
			end
		end
	end
end