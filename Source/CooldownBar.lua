--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not WCDBCooldownBar then WCDBCooldownBar = Frame:Subclass( "WCDBCooldownBar" ) end

local LSA					= LibStub( "LibSharedAssets" )
local DEFAULT_TEXTURE_NAME 	= "tint_square"
local DEFAULT_TEXTURE_SIZE	= { 32, 32 }

function WCDBCooldownBar:Create( windowName, parentName, abilityId, abilityType )
	local cooldownBar
	
	if( DoesWindowExist( windowName ) ) then
		cooldownBar = GetFrame( windowName )	
	else
		cooldownBar = self:CreateFromTemplate( windowName, parentName )
	end

	if( cooldownBar == nil ) then
		WCDB.Debug( "Error creating cooldown bar!" )
        return nil
    end
    
    local abilityData = Player.GetAbilityData( abilityId )
	
	if( abilityData == nil ) then
		WCDB.Debug( "Error retrieving ability data!" )
		return nil
	end
	
	--
    -- Initialize/store the arguments we need
    --
    cooldownBar.db 						= WCDB.db
    cooldownBar.m_WindowName 			= windowName
    cooldownBar.m_ParentName 			= parentName
    cooldownBar.m_AbilityId				= abilityId
    cooldownBar.m_AbilityType 			= abilityType
    cooldownBar.m_TimeRemaining			= 0
    cooldownBar.m_Cooldown				= 0
    cooldownBar.m_MaxCooldown			= 0
    cooldownBar.m_Showing				= false
    cooldownBar.m_Scale					= cooldownBar.db.anchor.scale
   	cooldownBar.m_FillWidth				= 0 
   	cooldownBar.m_FillHeight			= 0
   	
	--
    -- Store the ability data information
    --
    cooldownBar.m_AbilityIcon, _, _, _	= GetIconData( abilityData.iconNum )
    cooldownBar.m_AbilityName			= abilityData.name
    
    --
    -- Initialize our UI elements
    --
    DynamicImageSetTexture( cooldownBar:GetWindowName() .. "AbilityIcon", cooldownBar.m_AbilityIcon, 0, 0 )
    LabelSetText( cooldownBar:GetWindowName() .. "StatusAbilityName", cooldownBar.m_AbilityName )
    
    cooldownBar:ForceProcessAnchors()
    
    return cooldownBar
end

function WCDBCooldownBar:Destroy()
	WindowSetShowing( self:GetWindowName(), false )
	
	self:ParentDestroy()
end

function WCDBCooldownBar:Reset()
	local config = self.db.abilities.actions
	local showIcon = true
	local foregroundAnchor, abilityNameAnchor, abilityNameRelative, iconDim
	local fgColor, fgAlpha, bgColor, bgAlpha, fontColor, fontAlpha

	-- Hide ourself
	self:SetShowing( false )

	-- Get bar type based configuration
	if( self:GetAbilityType() == Player.AbilityType.MORALE ) then
		config = self.db.abilities.morale
	elseif( self:GetAbilityType() == Player.AbilityType.PET ) then
		config = self.db.abilities.pet
	end
	
	-- Hide our icon if need be
	WindowSetShowing( self:GetWindowName() .. "AbilityIcon", config.icon.show )

	-- Set the dimensions of the window
	WindowSetScale( self:GetWindowName(), self.m_Scale )
	WindowSetDimensions( self:GetWindowName(), self.db.bar.width / InterfaceCore.GetScale() , self.db.bar.height / InterfaceCore.GetScale() )
	
	-- Anchor the icon if need be, then move the status area accordingly
	if( showIcon ) then
		iconDim = math.min( self.db.bar.width / InterfaceCore.GetScale(), self.db.bar.height / InterfaceCore.GetScale() )
		WindowClearAnchors( self:GetWindowName() .. "AbilityIcon" )
		WindowSetDimensions( self:GetWindowName() .. "AbilityIcon", iconDim, iconDim )
		WindowAddAnchor( self:GetWindowName() .. "AbilityIcon", "left", self:GetWindowName(), "left", 0, 0 )
		
		WindowClearAnchors( self:GetWindowName() .. "Status" )
		WindowAddAnchor( self:GetWindowName() .. "Status", "topright", self:GetWindowName() .. "AbilityIcon", "topleft", 0, 0 )
		WindowAddAnchor( self:GetWindowName() .. "Status", "bottomright", self:GetWindowName(), "bottomright", 0, 0 )
	else
		WindowClearAnchors( self:GetWindowName() .. "Status" )
		WindowAddAnchor( self:GetWindowName() .. "Status", "topleft", self:GetWindowName(), "topleft", 0, 0 )
		WindowAddAnchor( self:GetWindowName() .. "Status", "bottomleft", self:GetWindowName(), "bottomleft", 0, 0 )
	end
	
	-- Anchor the ability name
	WindowClearAnchors( self:GetWindowName() .. "StatusAbilityName" )
	WindowAddAnchor( self:GetWindowName() .. "StatusAbilityName", "left", self:GetWindowName() .. "Status", "left", 5, 1 )
	
	-- Anchor the time remaining
	WindowClearAnchors( self:GetWindowName() .. "StatusRemaining" )
	WindowAddAnchor( self:GetWindowName() .. "StatusRemaining", "right", self:GetWindowName() .. "Status", "right", -5, 1 )
	
	-- Anchor our foreground
	WindowClearAnchors( self:GetWindowName() .. "StatusForeground" )
	if( self.db.bar.depletion == WCDBConstants.DEPLETION_RIGHT ) then
		WindowAddAnchor( self:GetWindowName() .. "StatusForeground", "topright", self:GetWindowName() .. "Status", "topright", 0, 0 )
		WindowAddAnchor( self:GetWindowName() .. "StatusForeground", "bottomright", self:GetWindowName() .. "Status", "bottomright", 0, 0 )	
	else
		WindowAddAnchor( self:GetWindowName() .. "StatusForeground", "topleft", self:GetWindowName() .. "Status", "topleft", 0, 0 )
		WindowAddAnchor( self:GetWindowName() .. "StatusForeground", "bottomleft", self:GetWindowName() .. "Status", "bottomleft", 0, 0 )
	end
	
	-- Set the texture of our elements
	local textureMeta = LSA and LSA:GetMetadata( self.db.bar.texture )
	local textureName, textureSize = DEFAULT_TEXTURE_NAME, DEFAULT_TEXTURE_SIZE
	
	if( textureMeta ~= nil ) then
		textureName = self.db.bar.texture
		textureSize = textureMeta.size
	end
	
	DynamicImageSetTexture( self:GetWindowName() .. "StatusForeground", textureName, 0, 0 )
	DynamicImageSetTexture( self:GetWindowName() .. "StatusBackground", textureName, 0, 0 )
	
	DynamicImageSetTextureDimensions( self:GetWindowName() .. "StatusForeground", unpack( textureSize ) )
	DynamicImageSetTextureDimensions( self:GetWindowName() .. "StatusBackground", unpack( textureSize ) )
	
	-- Set the tints of our elements
	WindowSetTintColor( self:GetWindowName() .. "StatusForeground", unpack( config.fg.color ) )
	WindowSetTintColor( self:GetWindowName() .. "StatusBackground", unpack( config.bg.color ) )
	
	WindowSetAlpha( self:GetWindowName() .. "StatusForeground", config.fg.alpha )
	WindowSetAlpha( self:GetWindowName() .. "StatusBackground", config.bg.alpha )
	WindowSetAlpha( self:GetWindowName() .. "AbilityIcon", config.icon.alpha )
	
	WindowSetFontAlpha( self:GetWindowName() .. "StatusAbilityName", config.font.alpha )
	WindowSetFontAlpha( self:GetWindowName() .. "StatusRemaining", config.font.alpha )
	
	LabelSetTextColor( self:GetWindowName() .. "StatusAbilityName", unpack( config.font.color ) )
	LabelSetTextColor( self:GetWindowName() .. "StatusRemaining", unpack( config.font.color ) )
		
	-- Reset our fillwidth
	self.m_FillWidth, self.m_FillHeight = WindowGetDimensions( self:GetWindowName() .. "Status" )
	
	-- Reprocess our anchors
	self:ForceProcessAnchors()
end

function WCDBCooldownBar:SetCooldown( cooldown, maxCooldown )
	self.m_Cooldown = cooldown
	self.m_MaxCooldown = maxCooldown
	
	self:UpdateCooldown( 0 )
end

function WCDBCooldownBar:UpdateCooldown( timeElapsed )
	-- Update our cooldown time
	self.m_Cooldown = self.m_Cooldown - timeElapsed
	
	local timeRemaining = self.m_Cooldown
	
	-- Minimum to 0
	if( timeRemaining < 0 ) then timeRemaining = 0 end
	
	-- Check to see if the status bar updates smooth
	if( not self.db.bar.smooth ) then
		timeRemaining = math.floor( timeRemaining ) 
	end
	
	-- Only update the display if we need to
	if( self.m_TimeRemaining ~= timeRemaining ) then
		-- Update our status bar
		local fillVal = math.min( ( timeRemaining / self.m_MaxCooldown ) * self.m_FillWidth, self.m_FillWidth )
			
		WindowSetDimensions( self:GetWindowName() .. "StatusForeground", fillVal, self.m_FillHeight )
		
		-- Update our remaining time display
		local remainingTime = TimeUtils.FormatSeconds( self.m_TimeRemaining, true )
		LabelSetText( self:GetWindowName() .. "StatusRemaining", remainingTime )
		
		self.m_TimeRemaining = timeRemaining
	end
end

function WCDBCooldownBar:SetShowing( showing )
	self:Set( "m_Showing", showing )
	WindowSetShowing( self:GetWindowName(), showing )
end

function WCDBCooldownBar:GetWindowName()
	return self:Get( "m_WindowName" )
end

function WCDBCooldownBar:GetAbilityName()
	return self:Get( "m_AbilityName" )
end

function WCDBCooldownBar:GetAbilityType()
	return self:Get( "m_AbilityType" )
end

function WCDBCooldownBar:GetMaxCooldown()
	return self:Get( "m_MaxCooldown" )
end

function WCDBCooldownBar:GetCooldown()
	return self:Get( "m_Cooldown" )
end

function WCDBCooldownBar:GetShowing()
	return self:Get( "m_Showing" )
end

function WCDBCooldownBar:Get( member )
	local value = self[member]
	return value
end

function WCDBCooldownBar:Set( member, value )
	self[member] = value
	return value
end
