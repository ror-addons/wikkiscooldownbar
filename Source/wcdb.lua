--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not WCDB then WCDB = {} end

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local alpha = false
--[===[@alpha@
alpha = true
--@end-alpha@]===]

local T 				= LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "wcdb", debug )
local LSA				= LibStub( "LibSharedAssets" )

WCDB.db					= nil
local db				= nil	

local VERSION 			= { MAJOR = 2, MINOR = 1, REV = 0 }
local DisplayVersion 	= string.format( "%d.%d.%d", VERSION.MAJOR, VERSION.MINOR, VERSION.REV )

if( debug ) then
	DisplayVersion 			= DisplayVersion .. " Dev Build"
else
	DisplayVersion 			= DisplayVersion .. " r" .. tonumber( "42" )
	
	if( alpha ) then
		DisplayVersion = DisplayVersion .. "-alpha"
	end
end

local firstLoad 				= true

local upgradeProfile

local hasPet					= false
local lastOnUpdate				= 0
local nextOnUpdate				= 0
local onUpdateRate				= 0

local cooldownBars				= {}
local blackList					= {}
local whiteList					= {}
local sortedCooldownBars	 	= {}

local anchorWindow 				= "WCDBAnchorFrame"
local WCDB_BASE_COOLDOWN_WINDOW	= "WCDB_COOLDOWNBAR_"

local _lastMoraleAbilityCast	= -1

local GENERAL_GCD					= 1.61

local ValidOrderingValueTypes =
{
    ["number"]  = true, 
    ["string"]  = true, 
    ["wstring"] = true, 
    ["boolean"] = true
}

local SortKeys = 
{
	[WCDBConstants.SORT_COOLDOWN] = 
	{
		["m_Cooldown"] 		= { fallback = "m_MaxCooldown" },
		["m_MaxCooldown"] 	= { fallback = "m_AbilityName" },
		["m_AbilityName"] 	= {}
	},
	
	[WCDBConstants.SORT_NAME] = 
	{
		["m_AbilityName"] 	= { fallback = "m_Cooldown" },
		["m_Cooldown"] 		= { fallback = "m_MaxCooldown" },
		["m_MaxCooldown"] 	= {},
	},
}

WCDB.DefaultSettings =
{
	general =
	{
		debug					= false,
		version					= VERSION,
		testmode				= false,
		enablewhitelist			= false,
		enableblacklist			= false,
	},
	
	bar =
	{
		width						= 185,
		height						= 15, 
		smooth						= true,
		depletion					= WCDBConstants.DEPLETION_LEFT,
		sort						= WCDBConstants.ORDER_DESCENDING,
		sorttype					= WCDBConstants.SORT_COOLDOWN,
		grow						= WCDBConstants.GROW_DOWN,
		texture						= "wcdb_tint_square",
	},
	
	anchor = 
	{
		point						= "center",
		relwin						= "Root",
		relpoint					= "center",
		scale						= 1.0,
		x							= 0,
		y							= 0,					
	},
	
	blacklist						= {},
	whitelist						= {},
	
	abilities =
	{
		actions =
		{
			mincooldown				= 4,
			enabled					= true,
			icon =
			{
				show				= true,
				alpha				= 1.0,
			},
			bg =
			{
				color 				= {32, 62, 24},
				alpha				= .4,
			},
			fg =
			{
				color 				= {85, 188, 70},
				alpha				= .7,
			},
			font =
			{
				color				= {255, 255, 255},
				alpha				= 1.0,
			},
		},
		
		morale =
		{
			mincooldown				= 4,
			enabled					= true,
			icon =
			{
				show				= true,
				alpha				= 1.0,
			},
			bg =
			{
				color 				= {32, 62, 24},
				alpha				= .4,
			},
			fg =
			{
				color 				= {85, 188, 70},
				alpha				= .7,
			},
			font =
			{
				color				= {255, 255, 255},
				alpha				= 1.0,
			},
		},
		
		pet =
		{
			mincooldown				= 4,
			enabled					= false,
			icon =
			{
				show				= true,
				alpha				= 1.0,
			},
			bg =
			{
				color 				= {32, 62, 24},
				alpha				= .4,
			},
			fg =
			{
				color 				= {85, 188, 70},
				alpha				= .7,
			},
			font =
			{
				color				= {255, 255, 255},
				alpha				= 1.0,
			},
		},
	},
}

function WCDB.GetVersion() return DisplayVersion end

function WCDB.OnInitialize()
	RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE, "WCDB.OnLoad" )
	RegisterEventHandler( SystemData.Events.LOADING_END, "WCDB.OnLoad" )
end

function WCDB.OnLoad()
	if( firstLoad ) then
		-- Create our configuration objects
		WCDB.InitProfiles()
		
		local profileIdx, _ = WCDB.GetCurrentProfile()
		
		-- Update our local db with configuration information
		WCDB.db = WCDB.GetProfileDataForUse( profileIdx )
		db = WCDB.db
		
		-- Upgrade our profile
		upgradeProfile()
		
		-- Register our slash commands with LibSlash
		WCDB.Debug( "Registering slash commands" )
		
		-- Load our configuration window
		WCDBConfig.OnLoad()
	
		-- Update the anchor window
		WCDB.UpdateAnchorWindow()
		
		-- Register to know when the layout editor is complete
    	table.insert( LayoutEditor.EventHandlers, WCDB.OnLayoutEditorFinished )
		
		-- Register to know when a player is casting a spell
		RegisterEventHandler( SystemData.Events.PLAYER_BEGIN_CAST, "WCDB.OnPlayerBeginCast" )
		
		--
		-- In order for us to get ability cooldowns, we need to snag this function for the data
		--
		WCDB.__ActionButton_UpdateCooldownAnimation					= ActionButton.UpdateCooldownAnimation
		ActionButton.UpdateCooldownAnimation 						= WCDB.OnAbilityUpdate
		
		--
		-- In order for us to get morale level timers, we need to snag this function for the data
		--
		WCDB.__MoraleButton_Update									= MoraleButton.Update
		MoraleButton.Update					 						= WCDB.OnMoraleUpdate
	
		-- Print our initialization usage
		local init = WStringToString( T["Wikki's Cooldown Bar %s initialized."] )
		WCDB.Print( init:format( DisplayVersion ) )
		
		-- Attempt to register one of our handlers
		if( LibSlash ~= nil and type( LibSlash.RegisterSlashCmd ) == "function" ) then
			LibSlash.RegisterSlashCmd( "wcdb", WCDB.Slash )
			LibSlash.RegisterSlashCmd( "wcdbconfig", WCDB.Slash )
			LibSlash.RegisterSlashCmd( "showwcdb", WCDB.Slash )
			WCDB.Print( T["(Wikki's Cooldown Bar) use /wcdb to bring up the configuration window."] )
		end
		
		d( "Wikki's Cooldown Bar loaded." )
	
		firstLoad = false
	end
	
	-- Check if we have a pet
	hasPet = GameData.Player.Pet.name ~= L"" 
	
	-- Every time we zone, reinitialize our bars
	WCDB.Reinitialize()
end

function WCDB.Reinitialize()
	-- Destroy any cooldown bars we have
	for index, bar in pairs( cooldownBars )
	do	
		bar:Destroy()
	end
	
	-- Clear our bar lookups
	cooldownBars 		= {}
	sortedCooldownBars 	= {}
	
	-- Rebuild our blacklist
	blackList = {}
	if( db.general.enableblacklist ) then
		for idx, ability in ipairs( db.blacklist )
		do
			blackList[ability] = true
		end
	end
	
	-- Rebuild our whitelist
	whiteList = {}
	if( db.general.enablewhitelist ) then
		for idx, ability in ipairs( db.whitelist )
		do
			whiteList[ability] = true
		end
	end
end

function WCDB.OnProfileChanged()
	local profileIdx, currentProfileName = WCDB.GetCurrentProfile()
	
	-- Update our local db with configuration information
	WCDB.db = WCDB.GetProfileDataForUse( profileIdx )
	db = WCDB.db
	
	-- Upgrade our profile
	upgradeProfile()
	
	-- Update the anchor window
	WCDB.UpdateAnchorWindow()
	
	WCDB.Reinitialize()
	
	WCDB.Print( L"(WCDB) " .. T["Active Profile:"] .. L" '" .. currentProfileName .. L"'" )
end

function WCDB.UpdateAnchorWindow()
	-- Remove our window from the layout editor
	LayoutEditor.UnregisterWindow( anchorWindow )
		
	-- Clear any anchors the window has
	WindowClearAnchors( anchorWindow )

	-- Set the anchor for its position on the screen
	WindowAddAnchor( anchorWindow,
		db.anchor.point,
		db.anchor.relwin,
		db.anchor.relpoint,
		db.anchor.x,
		db.anchor.y )
	
	-- Set the window's scale
	WindowSetScale( anchorWindow, db.anchor.scale )
	
	-- Set the dimensions
	WindowSetDimensions( anchorWindow, db.bar.width / InterfaceCore.GetScale(), db.bar.height / InterfaceCore.GetScale() )
	
	-- Register with the layout editor
 	LayoutEditor.RegisterWindow( anchorWindow, L"WCDB Anchor", L"Wikki's Cooldown Bar Anchor", true, true, true, nil )
end

function WCDB.OnLayoutEditorFinished( editorCode )
	if( editorCode == 2 ) then
		local point, relpoint, relwin, x, y 	= WindowGetAnchor( anchorWindow, 1 )
		local dx, dy							= WindowGetDimensions( anchorWindow )
		
		db.anchor.point 	= point
		db.anchor.relwin 	= relwin
		db.anchor.relpoint 	= relpoint
		db.anchor.x 		= x
		db.anchor.y 		= y
		
		db.anchor.scale 	= WindowGetScale( anchorWindow )
		
		db.bar.width 		= dx * InterfaceCore.GetScale() 
		db.bar.height 		= dy * InterfaceCore.GetScale()
		
		WCDB.Reinitialize()
	end
end

function WCDB.OnUpdate( timeElapsed )
	-- Only do this check if pet abilities are displayed
	if( firstLoad == false ) then
		lastOnUpdate = lastOnUpdate + timeElapsed
		
		-- Throttle this check	
		if( lastOnUpdate > onUpdateRate ) then
			local barHidden = false
			
			
			-- Update the time on the cooldown bars
			for _, bar in pairs( cooldownBars )
			do
				if( bar:GetShowing() ) then
					-- Check if we need to hide stale pet abilities
					if( db.abilities.pet.enabled and bar:GetAbilityType() == Player.AbilityType.PET ) then
						local prevHasPet = hasPet
						hasPet = GameData.Player.Pet.name ~= L""  
						if( prevHasPet == true and hasPet == false ) then
							bar:SetShowing( false )
							barHidden = true
							break
						end
					end
				
					-- Update our cooldown time
					bar:UpdateCooldown( lastOnUpdate )
					
					-- If the cooldown goes to -2 hide the bar
					if( bar:GetCooldown() < -2 ) then
						bar:SetShowing( false )
						barHidden = true
					end
				end
			end
			
			-- Only update display if we hide a bar
			if( barHidden ) then
				WCDB.UpdateCooldownBarDisplay( false )
			end
			
			lastOnUpdate = 0
		end	
	end
end

function WCDB.OnPlayerBeginCast( abilityId, isChannel, desiredCastTime, averageLatency )
	if( desiredCastTime == 0 ) then
		local abilityData = Player.GetAbilityData( abilityId )
		if( abilityData ~= nil and abilityData.abilityType == Player.AbilityType.MORALE ) then
			WCDB.Debug( "Storing morale ability id: " .. tostring( abilityId ) )
			_lastMoraleAbilityCast = abilityId
		end
	end
end

function WCDB.OnAbilityUpdate( ... )
	local self = ...
	local initialMaxCooldown = self.m_MaxCooldown
	
	-- Call the original function
	WCDB.__ActionButton_UpdateCooldownAnimation( ... )
	
	-- Perform the ability update
	WCDB.PerformAbilityUpdate( self.m_ActionId, self.m_Cooldown, initialMaxCooldown, self.m_MaxCooldown )
end

function WCDB.OnMoraleUpdate( ... )
	local self = ...
	local initialMaxCooldown = self.m_MaxCooldown
	
	-- Call the original function
	WCDB.__MoraleButton_Update( ... )
	
	--
	-- All morale abilities come off of cooldown at the same time, check to see if this was the morale ability fired
	--
	if( _lastMoraleAbilityCast ~= self.m_AbilityId ) then return end
	
	-- Perform the ability update
	WCDB.PerformAbilityUpdate( self.m_AbilityId, self.m_Cooldown, initialMaxCooldown, self.m_MaxCooldown )
end

function WCDB.PerformAbilityUpdate( abilityId, cooldown, initialMaxCooldown, currentMaxCooldown )
	if( abilityId == nil or cooldown == nil or initialMaxCooldown == nil or currentMaxCooldown == nil ) then return end
	
	-- If the max cooldown less than GCD return 
	if( initialMaxCooldown <= GENERAL_GCD ) then return end
	
	--
	-- If the ability is in our black list, we are not making a bar for it
	--
	if( blackList[abilityId] ~= nil ) then return end
	
	--
	-- Check if we have a cooldown bar for this ability
	--
	if( cooldownBars[abilityId] == nil ) then
	
		local abilityData = Player.GetAbilityData( abilityId )
		
		if( abilityData == nil ) then
			blackList[abilityId] = true
			return
		end
		
		-- If the ability isnt white listed check to see if we can white/black list it
		if( whiteList[abilityId] == nil ) then
			if( db.general.enablewhitelist and not whiteList[abilityId] ) then
				return
			else
				-- Check ability type and min cooldown accordingly
				if( ( abilityData.abilityType == Player.AbilityType.ABILITY and ( not db.abilities.actions.enabled or db.abilities.actions.mincooldown > initialMaxCooldown  ) ) or 
					( abilityData.abilityType == Player.AbilityType.MORALE and ( not db.abilities.morale.enabled or db.abilities.morale.mincooldown > initialMaxCooldown  ) ) or
					( abilityData.abilityType == Player.AbilityType.PET and ( not db.abilities.pet.enabled or db.abilities.pet.mincooldown > initialMaxCooldown  ) ) ) then
					-- We do not need a bar for this ability so black list it
					blackList[abilityId] = true
					return
				end
				
				-- If we got here, whitelist the ability
				whiteList[abilityId] = true
			end
		end
		
		local bar = WCDBCooldownBar:Create( WCDB_BASE_COOLDOWN_WINDOW .. tostring( abilityId ), anchorWindow, abilityId, abilityData.abilityType )
		
		if( bar == nil ) then WCDB.Debug( "Error creating WCDBCooldownBar!" ) return end
		
		-- Store our bar in the ability lookup
		cooldownBars[abilityId] = bar
		
		-- Store the bar in our sorted list
		table.insert( sortedCooldownBars, bar )
		
		-- Reset the coolbar
		cooldownBars[abilityId]:Reset()
	end
	
	local updateDisplay = false
	local resort = false
	
	--
	-- Check if this bar is visible but cooldown expired
	--
	if( cooldownBars[abilityId]:GetShowing() and cooldown <= 0 ) then
		cooldownBars[abilityId]:SetShowing( false )
		updateDisplay = true
	end

	--
	-- Check if this bar is not visible and is now on cooldown
	--	
	if( not cooldownBars[abilityId]:GetShowing() and cooldown > 0 and currentMaxCooldown > 0 ) then
		cooldownBars[abilityId]:SetCooldown( cooldown, currentMaxCooldown )
		cooldownBars[abilityId]:SetShowing( true )
		updateDisplay = true
		resort = true
	end
	
	-- Update our display if need be
	if( updateDisplay ) then
		WCDB.UpdateCooldownBarDisplay( resort )
	end
end

function WCDB.SortCooldownBars()
	table.sort( sortedCooldownBars, WCDB.__SortCooldownBars )	
end

function WCDB.UpdateCooldownBarDisplay( sort )
	local currentAnchor 			= {}
	
	currentAnchor.Point             = "topleft"
	currentAnchor.RelativePoint     = "topleft"
	currentAnchor.RelativeTo        = anchorWindow
	currentAnchor.XOffset           = 0
	currentAnchor.YOffset           = 0 
	
	-- Sort our bars if nee
	if( sort ) then	WCDB.SortCooldownBars()	end
	
	-- Reanchor the bars
	for index, bar in ipairs( sortedCooldownBars )
	do	
		-- If the bar has a cooldown remaining, reanchor the bar
		if( bar:GetShowing() ) then
			-- Remove the old anchors
			bar:ClearAnchors()
			
			-- Set the current anchor
			bar:SetAnchor( currentAnchor )
		
			-- Update the anchor for the next bar
			currentAnchor.RelativeTo = bar:GetWindowName()
			if( db.bar.grow == WCDBConstants.GROW_DOWN ) then
				currentAnchor.Point 			= "bottomleft"
				currentAnchor.RelativePoint 	= "topleft"
			else
				currentAnchor.Point 			= "topleft"
				currentAnchor.RelativePoint 	= "bottomleft"
			end
		end
	end
end

function WCDB.RefreshBars()
	for index, bar in pairs( cooldownBars )
	do	
		bar:Reset()
	end
end

function WCDB.__SortCooldownBars( bar1, bar2 )
	return WCDB.OrderingFunction( bar1, bar2, "m_Cooldown", SortKeys[db.bar.sorttype], db.bar.sort )
end

function WCDB.OrderingFunction( table1, table2, sortKey, sortKeys, sortOrder )
    local value1			= table1:Get( sortKey )
    local value2			= table2:Get( sortKey )
    local value1Type    	= type (value1)
    
    if (value1Type ~= type (value2) or not ValidOrderingValueTypes[value1Type])
    then
        return false
    end
    
    if (value1Type == "boolean")
    then
        local function NumberFromBoolean (b) 
            if (b) 
            then 
                return 1 
            end 
            return 0 
        end
        
        value1 = NumberFromBoolean (value1)
        value2 = NumberFromBoolean (value2)
    end
    
    assert (type (sortKeys[sortKey]) == "table")
    
    if (sortKeys[sortKey].isNumeric)
    then
        value1 = tonumber (value1)
        value2 = tonumber (value2)
    end
    
    if (value1 == value2)
    then
        local fallback = sortKeys[sortKey].fallback
        
        if (fallback)
        then
            return WCDB.OrderingFunction (table1, table2, fallback, sortKeys, sortOrder)
        end
    else
        if (sortOrder == WCDBConstants.ORDER_ASCENDING )
        then
            return value1 < value2
        end

        return value1 > value2    
    end
    
    return false
end

function WCDB.Slash( input )
	WCDB.ToggleConfig()
end

function WCDB.ToggleConfig()
	if( DoesWindowExist( WCDBConfig.GetWindowName() ) ) then
		WindowSetShowing( WCDBConfig.GetWindowName(), not WindowGetShowing( WCDBConfig.GetWindowName() ) )
	end
end

function WCDB.Print( str )
	EA_ChatWindow.Print( towstring( tostring( str ) ), ChatSettings.Channels[SystemData.ChatLogFilters.SAY].id );
end

function WCDB.Debug(str)
	if( db.general.debug ) then
	 	DEBUG( towstring( str ) )
	end
end

function upgradeProfile()
	local oldVersion = db.general.version
	db.general.version = VERSION
	
	-- add any processing here on version change
end