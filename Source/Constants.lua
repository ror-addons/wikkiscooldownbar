--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if not WCDBConstants then WCDBConstants = {} end

local WCDBConstants = WCDBConstants

local pairs 		= pairs
local ipairs		= ipairs
local tsort			= table.sort
local tinsert		= table.insert
local string_format = string.format

local T 	= LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "wcdb" )
local LSA 	= LibStub( "LibSharedAssets" )

WCDBConstants.SORT_COOLDOWN 				= 1
WCDBConstants.SORT_NAME		 				= 2
WCDBConstants.SORT_TYPES =
{
	[WCDBConstants.SORT_COOLDOWN]			= { name=T["Ability Cooldown"] },
	[WCDBConstants.SORT_NAME]				= { name=T["Ability Name"] },
}

WCDBConstants.ORDER_ASCENDING 				= 1
WCDBConstants.ORDER_DESCENDING 				= 2
WCDBConstants.ORDER_TYPES =
{
	[WCDBConstants.ORDER_ASCENDING]			= { name=T["Ascending"] },
	[WCDBConstants.ORDER_DESCENDING]		= { name=T["Descending"] },
}

WCDBConstants.GROW_UP 						= 1
WCDBConstants.GROW_DOWN						= 2
WCDBConstants.GROWTH_TYPES =
{
	[WCDBConstants.GROW_UP]					= { name=T["Up"] },
	[WCDBConstants.GROW_DOWN]				= { name=T["Down"] },
}

WCDBConstants.DEPLETION_LEFT 				= 1
WCDBConstants.DEPLETION_RIGHT				= 2
WCDBConstants.DEPLETION_TYPES =
{
	[WCDBConstants.DEPLETION_LEFT]			= { name=T["Left"] },
	[WCDBConstants.DEPLETION_RIGHT]			= { name=T["Right"] },
}

-- Texture Configuration Information
WCDBConstants.Textures		= {}
WCDBConstants.TextureNames	= {}

if LSA then
	-- Retrieve the textures from LSA
	WCDBConstants.Textures = LSA:GetTextureList() or {}
	
	-- Sort the textures
	tsort( WCDBConstants.Textures )
	
	-- Build the texture name table
	for index, texture in ipairs( WCDBConstants.Textures )
	do
		-- Reverse the lookup
		WCDBConstants.Textures[texture] = index
		
		-- Attempt to retrieve any meta data for the texture
		local meta = LSA:GetMetadata( texture )
		
		if( meta and meta.displayname ) then
			WCDBConstants.TextureNames[index] = meta.displayname
		else
			WCDBConstants.TextureNames[index] = towstring( texture )
		end
	end
end