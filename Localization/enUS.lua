--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/wcdb/localization/
--

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "wcdb", "enUS", true, debug )

T["(Wikki's Cooldown Bar) use /wcdb to bring up the configuration window."] = L"(Wikki's Cooldown Bar) use /wcdb to bring up the configuration window."
T["(sec)"] = L"(sec)"
T["Ability Cooldown"] = L"Ability Cooldown"
T["Ability Name"] = L"Ability Name"
T["Actions"] = L"Actions"
T["Add Ability"] = L"Add Ability"
T["Alpha"] = L"Alpha"
T["Apply"] = L"Apply"
T["Are you sure?"] = L"Are you sure?"
T["Ascending"] = L"Ascending"
T["Background Texture Color"] = L"Background Texture Color"
T["Bar Depletion Direction:"] = L"Bar Depletion Direction:"
T["Blue"] = L"Blue"
T["Close"] = L"Close"
T["Colors"] = L"Colors"
T["Cooldown Bar Texture:"] = L"Cooldown Bar Texture:"
T["Copy Settings From:"] = L"Copy Settings From:"
T["Create"] = L"Create"
T["Create New Profile:"] = L"Create New Profile:"
T["Current Profile:"] = L"Current Profile:"
T["Delete"] = L"Delete"
T["Descending"] = L"Descending"
T["Down"] = L"Down"
T["Drag and drop abilities below and click 'Add Ability' to add them to the current filter."] = L"Drag and drop abilities below and click 'Add Ability' to add them to the current filter."
T["Enable Blacklist"] = L"Enable Blacklist"
T["Enable Filtering:"] = L"Enable Filtering:"
T["Enable Whitelist"] = L"Enable Whitelist"
T["Enable action cooldown monitoring"] = L"Enable action cooldown monitoring"
T["Enable morale cooldown monitoring"] = L"Enable morale cooldown monitoring"
T["Enable pet action cooldown monitoring"] = L"Enable pet action cooldown monitoring"
T["Filter"] = L"Filter"
T["Filtered Abilities:"] = L"Filtered Abilities:"
T["Font Color"] = L"Font Color"
T["Foreground Texture Color"] = L"Foreground Texture Color"
T["General"] = L"General"
T["Green"] = L"Green"
T["Growth Direction:"] = L"Growth Direction:"
T["Left"] = L"Left"
T["Minimum cooldown time to display:"] = L"Minimum cooldown time to display:"
T["Morale"] = L"Morale"
T["NOTE: Profile changes take effect immediately!"] = L"NOTE: Profile changes take effect immediately!"
T["None"] = L"None"
T["Pet Actions"] = L"Pet Actions"
T["Profiles"] = L"Profiles"
T["Red"] = L"Red"
T["Reset Profile"] = L"Reset Profile"
T["Revert"] = L"Revert"
T["Right"] = L"RIght"
T["Set Active Profile:"] = L"Set Active Profile:"
T["Show action icon"] = L"Show action icon"
T["Sort Direction:"] = L"Sort Direction:"
T["Up"] = L"Up"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"WARNING: Clicking this button will restore the current profile back to default values!"
T["Wikki's Cooldown Bar %s initialized."] = L"Wikki's Cooldown Bar %s initialized."
T["Wikki's Cooldown Bar - Configuration"] = L"Wikki's Cooldown Bar - Configuration"
