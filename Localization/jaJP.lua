--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/wcdb/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("wcdb", "jaJP")
if not T then return end

-- T["(Wikki's Cooldown Bar) use /wcdb to bring up the configuration window."] = L""
-- T["(sec)"] = L""
-- T["Ability Cooldown"] = L""
-- T["Ability Name"] = L""
-- T["Actions"] = L""
-- T["Add Ability"] = L""
-- T["Alpha"] = L""
-- T["Apply"] = L""
-- T["Are you sure?"] = L""
-- T["Ascending"] = L""
-- T["Background Texture Color"] = L""
-- T["Bar Depletion Direction:"] = L""
-- T["Blue"] = L""
-- T["Close"] = L""
-- T["Colors"] = L""
-- T["Cooldown Bar Texture:"] = L""
-- T["Copy Settings From:"] = L""
-- T["Create"] = L""
-- T["Create New Profile:"] = L""
-- T["Current Profile:"] = L""
-- T["Delete"] = L""
-- T["Descending"] = L""
-- T["Down"] = L""
-- T["Drag and drop abilities below and click 'Add Ability' to add them to the current filter."] = L""
-- T["Enable Blacklist"] = L""
-- T["Enable Filtering:"] = L""
-- T["Enable Whitelist"] = L""
-- T["Enable action cooldown monitoring"] = L""
-- T["Enable morale cooldown monitoring"] = L""
-- T["Enable pet action cooldown monitoring"] = L""
-- T["Filter"] = L""
-- T["Filtered Abilities:"] = L""
-- T["Font Color"] = L""
-- T["Foreground Texture Color"] = L""
-- T["General"] = L""
-- T["Green"] = L""
-- T["Growth Direction:"] = L""
-- T["Left"] = L""
-- T["Minimum cooldown time to display:"] = L""
-- T["Morale"] = L""
-- T["NOTE: Profile changes take effect immediately!"] = L""
-- T["None"] = L""
-- T["Pet Actions"] = L""
-- T["Profiles"] = L""
-- T["Red"] = L""
-- T["Reset Profile"] = L""
-- T["Revert"] = L""
-- T["Right"] = L""
-- T["Set Active Profile:"] = L""
-- T["Show action icon"] = L""
-- T["Sort Direction:"] = L""
-- T["Up"] = L""
-- T["WARNING: Clicking this button will restore the current profile back to default values!"] = L""
-- T["Wikki's Cooldown Bar %s initialized."] = L""
-- T["Wikki's Cooldown Bar - Configuration"] = L""

